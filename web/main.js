function btndvm(){
    var i = 0;
    var str="";
    var xml = new XMLHttpRequest();
    xml.open('GET', 'data.xml', false);
    xml.send();
    var xmldata = xml.responseText;

    xmldata = (new DOMParser()).parseFromString(xml.responseText, 'text/xml');
    var emp = xmldata.getElementsByTagName("book");

    for (var i = 0; i < emp.length; ++i) {

        var title = emp[i].getElementsByTagName("title")[0].firstChild.data;
        var author = emp[i].getElementsByTagName("author")[0].firstChild.data;
        var year = emp[i].getElementsByTagName("year")[0].firstChild.data;

        str += "<tr> <td>" + title + "</td> <td>" + author + "</td>" +
        "<td>" + year + "</td> </tr>";
    }

    document.getElementById("tablo").innerHTML = str;
}

function login() {

    var xml = new XMLHttpRequest();
    xml.open('GET', 'login.xml', false);
    xml.send();
    var xmldata = xml.responseXML;

    xmldata = (new DOMParser()).parseFromString(xml.responseText, 'text/xml');
    var emp = xmldata.getElementsByTagName("user");

    var nm= emp[0].getElementsByTagName("name")[0].firstChild.data;
    var name = emp[0].getElementsByTagName("uname")[0].firstChild.data;
    var pss = emp[0].getElementsByTagName("pss")[0].firstChild.data;
    var email = emp[0].getElementsByTagName("email")[0].firstChild.data;

    var getuname =document.getElementById("name").value;
    var getpass= document.getElementById("psw").value;


    if(getuname == name && getpass == pss){
        window.location.href="admin.html";
        window.localStorage.setItem("name",nm);
        window.localStorage.setItem("usr",name);
        window.localStorage.setItem("pss",pss);
        window.localStorage.setItem("eml",email);
        return true;
    }
    else{
        window.alert("Kullanıcı adı ve şifreyi kontrol edip tekrar deneyiniz....");
        return false;
    }
}

if (window.attachEvent) {window.attachEvent('onload', btndvm);}
else if (window.addEventListener) {window.addEventListener('load', btndvm, false);}
else {document.addEventListener('load', btndvm, false);}